namespace PomodoroApp {
  public partial class FormPomodoro : Form {
    public FormPomodoro() {
      InitializeComponent();
      workInProgress = true;
      seconds = 25 * 60;
    }

    public void RefreshClock() {
      lblClock.Text = string.Format("{0}:{1:00}", seconds / 60, seconds % 60);
    }

    public void RefreshStatus() {
      if (workInProgress) {
        lblStatus.Text = "Work in progress...";
        lblStatus.BackColor = Color.LightYellow;
      } else {
        lblStatus.Text = "Rest in progress...";
        lblStatus.BackColor = Color.PaleGreen;
      }
    }

    private void tmrClock_Tick(object sender, EventArgs e) {
      seconds--;
      if (seconds <= 0) {
        workInProgress = !workInProgress;
        if (workInProgress) {
          try {
            seconds = int.Parse(tbWork.Text) * 60;
          } catch (Exception) {
            // ukoliko nije moguce parsirati sadrzaj textboxa, potrebno je iskljuciti timer kako se ne bi
            // svake sekunde prikazivao messagebox
            tmrClock.Enabled = false;
            MessageBox.Show("Krivo uneseno vrijeme za posao!", "Pogre�ka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
        } else {
          try {
            seconds = int.Parse(tbRest.Text) * 60;
          } catch (Exception) {
            tmrClock.Enabled = false;
            MessageBox.Show("Krivo uneseno vrijeme za odmor!", "Pogre�ka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
        }
        RefreshStatus();
      }
      RefreshClock();
    }

    private void btnStartStop_Click(object sender, EventArgs e) {
      tmrClock.Enabled = !tmrClock.Enabled;
    }

    private void btnReset_Click(object sender, EventArgs e) {
      tmrClock.Enabled = false;
      workInProgress = true;
      try {
        seconds = int.Parse(tbWork.Text) * 60;
      } catch (Exception) {
        MessageBox.Show("Krivo uneseno vrijeme za posao!", "Pogre�ka!", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      RefreshClock();
    }

    private bool workInProgress;
    private int seconds;
  }
}